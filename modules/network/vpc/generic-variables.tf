variable "aws_region" {
  type    = string
  default = "eu-north-1"
}
###################################################
variable "environment" {
  type    = string
  default = "prod"
}
###################################################
variable "project" {
  type    = string
  default = "abc"
}
###################################################
variable "component" {
  type    = string
  default = "eks"
}