# VPC Resource
resource "aws_vpc" "main" {

  cidr_block = var.vpc_cidr

  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  tags = {
    Name = "${local.name}-vpc"
  }
}
#######################################################################################
# Internet Gateway Resource
resource "aws_internet_gateway" "gw" {
  
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${local.name}-gw"
  }
}
#######################################################################################
# Nat Gateway Resource
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public[1].id

  tags = {
    Name = "${local.name}-nat"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  # depends_on =  [ aws_internet_gateway.gw , aws_subnet.public[1] , aws_eip.eip ] 
}
########################################################################################
# EIP Resource
resource "aws_eip" "eip" {
  domain = "vpc"

  tags = {
    Name = "${local.name}-eip"
  }

  depends_on = [aws_internet_gateway.gw] 
}
########################################################################################
# Public Subnet Resource
resource "aws_subnet" "public" {
  count             = max(length(var.vpc_public_subnets))
  vpc_id            = aws_vpc.main.id
  cidr_block        = element(var.vpc_public_subnets, count.index)
  availability_zone = element(var.vpc_azs, count.index)

  tags = {
    Name = "${local.name}-${var.vpc_public_subnets[count.index]}"
    # "kubernetes.io/role/elb"              = 1
    # "kubernetes.io/cluster/${local.name}" = "shared"
  }
}
########################################################################################
# Route Table Resource For Public Subnets
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${local.name}-public-route-table"
  }
}
########################################################################################
# Route Table Association Resource For Public Sibnets
resource "aws_route_table_association" "public" {
  count          = max(length(var.vpc_public_subnets))
  subnet_id      = element(aws_subnet.public[*].id, count.index)
  route_table_id = aws_route_table.public.id
}
########################################################################################
# Route Resource For Public Subnets
resource "aws_route" "public_internet_gateway" {

  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id

  timeouts {
    create = "5m"
  }
}
#########################################################################################
# Private Subnet Resource
resource "aws_subnet" "private" {
  count             = max(length(var.vpc_private_subnets))
  vpc_id            = aws_vpc.main.id
  cidr_block        = element(var.vpc_private_subnets, count.index)
  availability_zone = element(var.vpc_azs, count.index)

  tags = {
    Name = "${local.name}-${var.vpc_private_subnets[count.index]}"
    # "kubernetes.io/role/internal-elb"     = 1
    # "kubernetes.io/cluster/${local.name}" = "shared"
  }
}
##########################################################################################
# Route Table Resource For Private
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${local.name}-private-route-table"
  }
}
##########################################################################################
# Route Table Association Resource For Private Subnet
resource "aws_route_table_association" "private" {
  count          = max(length(var.vpc_private_subnets))
  subnet_id      = element(aws_subnet.private[*].id, count.index)
  route_table_id = aws_route_table.private.id
}
##########################################################################################
# Route Resource For private subnets
resource "aws_route" "private_nat_gateway" {

  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id

  timeouts {
    create = "5m"
  }
}
###########################################################################################
# Network ACL Public
resource "aws_network_acl" "public" {
  vpc_id = aws_vpc.main.id
  subnet_ids = aws_subnet.public[*].id

  tags = {
    Name = "${local.name}-network-acl-public-subnets"
  }
}
##########################################################################################
# Network ACL Rule Inpound Resource
resource "aws_network_acl_rule" "public-Inpound" {
  count          = max(length(var.public_inbound_acl_rules)) 
  network_acl_id = aws_network_acl.public.id
  rule_number    = var.public_inbound_acl_rules[count.index]["rule_number"]
  egress         = false
  protocol       = var.public_inbound_acl_rules[count.index]["protocol"]
  rule_action    = var.public_inbound_acl_rules[count.index]["rule_action"]
  cidr_block     = lookup(var.public_inbound_acl_rules[count.index], "cidr_block", null)
  from_port      = lookup(var.public_inbound_acl_rules[count.index], "from_port", null)
  to_port        = lookup(var.public_inbound_acl_rules[count.index], "to_port", null)
}
############################################################################################
# Network ACL Rule Outpound Resource
resource "aws_network_acl_rule" "public-Outpound" {
  count = max(length(var.private_inbound_acl_rules))
  network_acl_id = aws_network_acl.public.id
  rule_number    = var.private_inbound_acl_rules[count.index]["rule_number"]
  egress         = true
  protocol       = var.private_inbound_acl_rules[count.index]["protocol"]
  rule_action    = var.private_inbound_acl_rules[count.index]["rule_action"]
  cidr_block     = lookup(var.private_inbound_acl_rules[count.index], "cidr_block", null)
  from_port      = lookup(var.private_inbound_acl_rules[count.index], "from_port", null)
  to_port        = lookup(var.private_inbound_acl_rules[count.index], "to_port", null)
}
#############################################################################################
# Network ACL Private
resource "aws_network_acl" "private" {
  vpc_id = aws_vpc.main.id
  subnet_ids = aws_subnet.private[*].id

  tags = {
    Name = "${local.name}-network-acl-private-subnets"
  }
}
##############################################################################################
# Network ACL Rule Inpound Resource - Private
resource "aws_network_acl_rule" "private-Inpound" {
  count          = max(length(var.private_inbound_acl_rules)) 
  network_acl_id = aws_network_acl.private.id
  rule_number    = var.private_inbound_acl_rules[count.index]["rule_number"]
  egress         = false
  protocol       = var.private_inbound_acl_rules[count.index]["protocol"]
  rule_action    = var.private_inbound_acl_rules[count.index]["rule_action"]
  cidr_block     = lookup(var.private_inbound_acl_rules[count.index], "cidr_block", null)
  from_port      = lookup(var.private_inbound_acl_rules[count.index], "from_port", null)
  to_port        = lookup(var.private_inbound_acl_rules[count.index], "to_port", null)
}
################################################################################################
# Network ACL Rule Outpound Resource - Private
resource "aws_network_acl_rule" "private-Outpound" {
  count = max(length(var.private_inbound_acl_rules))
  network_acl_id = aws_network_acl.private.id
  rule_number    = var.private_inbound_acl_rules[count.index]["rule_number"]
  egress         = true
  protocol       = var.private_inbound_acl_rules[count.index]["protocol"]
  rule_action    = var.private_inbound_acl_rules[count.index]["rule_action"]
  cidr_block     = lookup(var.private_inbound_acl_rules[count.index], "cidr_block", null)
  from_port      = lookup(var.private_inbound_acl_rules[count.index], "from_port", null)
  to_port        = lookup(var.private_inbound_acl_rules[count.index], "to_port", null)
}
#################################################################################################
# Security Group Resource - Cluster
resource "aws_security_group" "cluster" {

  name        = var.cluster_security_group_use_name
  description = "Security Group For EKS Cluster"
  vpc_id      = aws_vpc.main.id

  tags = {
    Name = "${local.name}-security-group-cluster"
  }

  lifecycle {
    create_before_destroy = true
  }
}
###############################################################################################
resource "aws_security_group_rule" "cluster" {
  count = max(length(var.security-rules-cluster))
  type              = lookup(var.security-rules-cluster[count.index] , "type", null)
  from_port         = lookup(var.security-rules-cluster[count.index] , "from_port" , null)
  to_port           = lookup(var.security-rules-cluster[count.index] , "to_port" , null)
  protocol          = lookup(var.security-rules-cluster[count.index] , "protocol" , null)
  cidr_blocks       = var.cidr_blocks_for_sg
  security_group_id = aws_security_group.cluster.id
}

