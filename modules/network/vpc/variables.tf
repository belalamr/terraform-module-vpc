variable "vpc_name" {
  type = string
}
########################################################################################
variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}
########################################################################################
variable "enable_dns_hostnames" {
  type = bool
}
########################################################################################
variable "enable_dns_support" {
  type = bool
}
########################################################################################
variable "create_igw" {
  type = bool
}
#########################################################################################
variable "vpc_public_subnets" {
  type = list(string)
  #    default = [ "10.0.101.0/24" , "10.0.102.0/24" , "10.0.103.0/24" ]
}
#########################################################################################
variable "vpc_private_subnets" {
  type = list(string)
  #    default = [ "10.0.101.0/24" , "10.0.102.0/24" , "10.0.103.0/24" ]
}
##########################################################################################
variable "vpc_azs" {
  type    = list(string)
  default = ["eu-north-1a", "eu-north-1b", "eu-north-1c"]
}
##########################################################################################
variable "public_inbound_acl_rules" {
  description = "Public subnets inbound network ACLs"
  type        = list(map(string))
  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
    {
      rule_number = 200
      rule_action = "allow"
      from_port   = 10
      to_port     = 20
      protocol    = "tcp"
      cidr_block  = "10.0.0.0/24"
    },
  ]
}
###########################################################################################
variable "private_inbound_acl_rules" {
  description = "Private subnets inbound network ACLs"
  type        = list(map(string))
  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
    {
      rule_number = 200
      rule_action = "allow"
      from_port   = 10
      to_port     = 20
      protocol    = "tcp"
      cidr_block  = "10.0.0.0/24"
    },
  ]
}
#############################################################################################
variable "cluster_security_group_use_name" {
  description = "Cluster Security group Name"
  type        = string
  default     = "cluster-eks-sg"
}
#############################################################################################
variable "security-rules-cluster" {
  description = "security rules for eks cluster"
  type        = list(map(string))
  default = [{
    "type"        = "ingress"
    "from_port"   = 443
    "to_port"     = 443
    "protocol"    = "tcp"
    },
    {
      "type"        = "egress"
      "from_port"   = 5000
      "to_port"     = 5050
      "protocol"    = "tcp"
  }]
}
###############################################################################################
variable "cidr_blocks_for_sg" {
  type = list(string)
  default = [ "10.0.101.0/24" , "10.0.102.0/24" , "10.0.103.0/24" ]
}
###############################################################################################