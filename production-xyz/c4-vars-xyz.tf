variable "aws_region" {
  type    = string
  default = "eu-north-1"
}
##########################################################
variable "environment" {
  type    = string
  default = "production"
}
###########################################################
variable "project" {
  type    = string
  default = "xyz"
}
###########################################################
variable "component" {
  type    = string
  default = "eks"
}
###########################################################
variable "enable_dns_hostnames" {
  type    = bool
  default = true
}
#############################################################
variable "enable_dns_support" {
  type    = bool
  default = true
}
#############################################################
variable "create_igw" {
  type    = bool
  default = true
}
#############################################################
variable "vpc_public_subnets" {
  type    = list(string)
  default = ["10.0.105.0/24", "10.0.106.0/24", "10.0.107.0/24", "10.0.108.0/24", "10.0.109.0/24"]
}
##############################################################
variable "vpc_private_subnets" {
  type    = list(string)
  default = ["10.0.115.0/24", "10.0.116.0/24", "10.0.117.0/24", "10.0.118.0/24", "10.0.119.0/24"]
}
###############################################################
variable "vpc_azs" {
  type    = list(string)
  default = ["eu-north-1a", "eu-north-1b", "eu-north-1c"]
}

