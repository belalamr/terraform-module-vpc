locals {
  owners      = var.project
  environment = var.environment
  name        = "${var.project}-${var.environment}-${var.component}"
}