terraform {
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.25"
    }
  }
  backend "http" {
    username = "belalamr"
  }
}

provider "aws" {
  region  = var.aws_region
#  profile = "default"
}
