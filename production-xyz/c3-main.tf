module "webserver_cluster" {
  source   = "../modules/network/vpc"
  vpc_name = "${local.name}-vpc"

  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  create_igw = var.create_igw

  vpc_public_subnets  = var.vpc_public_subnets
  vpc_azs             = var.vpc_azs
  vpc_private_subnets = var.vpc_private_subnets
}